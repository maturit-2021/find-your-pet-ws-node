# Our first stage, that is the Builder
FROM node:14 AS api-builder

# Create the app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./package*.json ./
RUN npm ci

COPY ./tsconfig*.json ./
COPY ./src src

RUN npm run build

# Our Second stage, that creates an image for production
FROM node:14-slim

ENV NODE_ENV production

# Create the app directory
RUN mkdir -p /usr/src/app

# Change the owner of the app directory
RUN chown node:node /usr/src/app
WORKDIR /usr/src/app

# Switch to user node
USER node

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY ./package*.json ./

RUN npm install --production && npm cache clean --force

# Bundle app source
COPY --from=api-builder ./usr/src/app/dist ./dist

EXPOSE 8080
CMD ["npm", "start"]