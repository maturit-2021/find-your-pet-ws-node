import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as csurf from 'csurf';
import * as morgan from 'morgan';

import "reflect-metadata";
import { TransformResponseInterceptor } from './lib/interceptors/handle_response.interceptor';
import { ConfigService } from '@nestjs/config';
import { SocketIoAdapter } from './lib/adapters/socket_io.adapter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  const configService = app.get(ConfigService);
  
  app.use(morgan('tiny'));
  app.use(helmet());
  // app.use(csurf());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalInterceptors(new TransformResponseInterceptor());
  
  app.useWebSocketAdapter(new SocketIoAdapter(app, '*'));
  
  await app.listen(
    configService.get('PORT'), 
    () => console.log(`Server is listening on port ${configService.get('PORT')}`)
  );
}

bootstrap();
