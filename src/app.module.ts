import { HealthController } from './controllers/health.controller';
import { EventsModule } from './modules/events/events.module';
import { PostModule } from './modules/post/post.module';
import { SightingModule } from './modules/sighting/sighting.module';
import { ProfileModule } from './modules/profile/profile.module';
import { PetModule } from './modules/pet/pet.module';
import { LostPetModule } from './modules/lost_pet/lostpet.module';
import { CommentModule } from './modules/comment/comment.module';
import { ChatModule } from './modules/chat/chat.module';
import { BeaconModule } from './modules/beacon/beacon.module';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtAuthGuard } from './modules/auth/guard/jwt_auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/config';
import { TerminusModule } from '@nestjs/terminus';
import { EnvironmentVariables } from './lib/interfaces/environment.interface';
import { MulterModule } from '@nestjs/platform-express';
import { MulterConfigService } from './services/multer_config.service';

@Module({
  imports: [
    TerminusModule,
    EventsModule,
    ConfigModule,
    PostModule,
    SightingModule,
    ProfileModule,
    PetModule,
    LostPetModule,
    CommentModule,
    ChatModule,
    BeaconModule,
    AuthModule,
    UsersModule,
    ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
      envFilePath: '.env',
      load: [configuration]
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService<EnvironmentVariables>) => config.get('database')
    }),
    MulterModule.registerAsync({
      useClass: MulterConfigService,
    })
  ],
  controllers: [
    HealthController,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    }
  ],
})
export class AppModule { }
