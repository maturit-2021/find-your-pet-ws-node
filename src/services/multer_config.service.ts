import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MulterModuleOptions, MulterOptionsFactory } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { EnvironmentVariables } from 'src/lib/interfaces/environment.interface';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
    constructor(
        private config: ConfigService<EnvironmentVariables>
    ){}
    createMulterOptions(): MulterModuleOptions {
        return this.config.get<MulterOptions>('multerOptions');
    }
}

         