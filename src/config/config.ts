import * as bytes from 'bytes';
import { DbLoggerService } from './dblogger.service';

export default () => ({
    NODE_ENV: process.env.ENV || 'development',
    environment: process.env.ENV || 'development',
    port: parseInt(process.env.PORT ?? '3000', 10),
    auth: {
        secret: process.env.AUTH_KEY,
        duration: process.env.AUTH_DURATION,
        duration_refresh: process.env.AUTH_REFRESH_DURATION,
    },
    database: {
        type: 'mysql',
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT, 10),
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        entities: ["dist/**/*.entity.js"],
        logging: process.env.ENV === 'development',
        logger: new DbLoggerService(process.env.ENV === 'development'),
        synchronize: process.env.ENV === 'development' || Boolean(process.env.TYPEORM_SYNC),
        autoLoadEntities: true,
    },
    multerOptions: {
        limits: {
            fileSize: bytes.parse(process.env.MAX_FILE_SIZE ?? '4MB')
        }
    }
});