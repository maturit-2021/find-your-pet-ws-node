import { AdvancedConsoleLogger, Logger as TypeOrmLogger, QueryRunner } from "typeorm";

export class DbLoggerService extends AdvancedConsoleLogger implements TypeOrmLogger {
    
    logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner) {
        const params = parameters?.map(p => {
            if (p.toString().length > 100)
                return `${p.slice(0, 10)}...`;
            return p;
        })
        super.logQuery(query, params, queryRunner);
    }

    logQueryError(error: string | Error, query: string, parameters?: any[], queryRunner?: QueryRunner) {
        super.logQueryError(error.toString(), query, parameters, queryRunner);
    }

    logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner) {
        super.logQuerySlow(time, query, parameters, queryRunner);
    }
}
