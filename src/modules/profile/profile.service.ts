import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { RegisterDto } from '../auth/dto/register.dto';
import { UserEntity } from '../users/user.entity';
import { UpdateProfileDto } from './dto/update_profile.dto';
import { ProfileEntity } from './profile.entity';

@Injectable()
export class ProfileService {
    constructor(
        private connection: Connection,
        @InjectRepository(ProfileEntity) private profileRepository: Repository<ProfileEntity>,
        @InjectRepository(UserEntity) private usersRepository: Repository<UserEntity>
    ) { }
    
    /**
     * Create a new profile and associate an user with the new profile
     * @param data - The data necessary to populate both Profile and User Entities
     */
    async createProfile(data: RegisterDto): Promise<UserEntity> {
        const queryRunner = this.connection.createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const profileEntity = new ProfileEntity();
            profileEntity.name = data.name;
            profileEntity.city = data.name;

            const userEntity = new UserEntity();
            userEntity.email = data.email;
            userEntity.password = data.password;
            userEntity.profile = profileEntity;

            await queryRunner.manager.save([profileEntity, userEntity])
            await queryRunner.commitTransaction();

            return userEntity;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw error;
            
        } finally {
            await queryRunner.release();
        }
    }

    /**
     * Retrieve the user profile information from the database
     * @param id - The ID of the requested profile
     */
    getProfile(id: string): Promise<ProfileEntity>{
        return this.profileRepository.findOne(id);
    }

    /**
     * Update the user profile information
     * @param id - The profile ID of the user
     * @param data - The data to be used for the update
     */
    async updateProfile(id: string, data: UpdateProfileDto): Promise<void>{
        const entity = new ProfileEntity({ id, ...data });
        await this.profileRepository.save(entity);
    }

    /**
     * Update the profile picture of the given id
     * @param id - The profile ID to be updated
     * @param image - The image to be used
     */
    async updatePicture(id: string, image: Buffer): Promise<void>{
        const entity = new ProfileEntity({ 
            id, 
            image: image ?? null
        });
        await this.profileRepository.save(entity);
    }
}
