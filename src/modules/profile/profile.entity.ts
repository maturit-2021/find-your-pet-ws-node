import { bufferToStringTransformer } from "src/lib/transformers/buffer_to_string.transformer";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ChatEntity } from "../chat/chat.entity";
import { CommentEntity } from "../comment/comment.entity";
import { PetEntity } from "../pet/pet.enitity";
import { PostEntity } from "../post/post.entity";
import { SightingEntity } from "../sighting/sighting.entity";

@Entity()
export class ProfileEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @Column()
    name: string;

    @Column()
    city: string;

    @Column({
        type: 'mediumblob',
        transformer: bufferToStringTransformer,
        default: null,
        nullable: true,
    })
    image?: Buffer;

    @OneToMany(_type =>  PetEntity, pet => pet.profile)
    pets: PetEntity[];

    @OneToMany(_type =>  ChatEntity, chat => chat.profileFrom)
    messagesSent: ChatEntity[];

    @OneToMany(_type =>  ChatEntity, chat => chat.profileTo)
    messagesReceived: ChatEntity[];

    @OneToMany(_type =>  PostEntity, post => post.profile)
    posts: PostEntity[];

    @OneToMany(_type =>  CommentEntity, cmt => cmt.profile)
    comments: CommentEntity[];

    @OneToMany(_type =>  SightingEntity, sg => sg.profile)
    sightings: SightingEntity[];

    constructor({id, name, city, image}: Partial<ProfileEntity> = {}){
        this.id = id;
        this.name = name;
        this.city = city;
        this.image = image;
    }
}