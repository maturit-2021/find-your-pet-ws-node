import { Body, Controller, Get, Put, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from 'src/lib/metada/user.metadata';
import { UserAuth } from '../auth/dto/jwt_payload.dto';
import { UpdateProfileDto } from './dto/update_profile.dto';
import { ProfileService } from './profile.service';

@Controller('profile')
export class ProfileController {
    constructor(
        private profileService: ProfileService
    ){}
    
    @Get()
    me(@User() user: UserAuth) {
        return this.profileService.getProfile(user.profileId);
    }

    @Put()
    updateProfile(@User() user: UserAuth, @Body() profile: UpdateProfileDto) {
        return this.profileService.updateProfile(user.profileId, profile);
    }

    @Put('picture')
    @UseInterceptors(FileInterceptor('image'))
    updateProfilePicture(@User() user: UserAuth, @UploadedFile() file: Express.Multer.File) {
        return this.profileService.updatePicture(user.profileId, file?.buffer);
    }
}
