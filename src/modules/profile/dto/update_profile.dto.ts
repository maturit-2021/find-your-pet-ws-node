import { IsNotEmpty } from "class-validator";
import { ProfileEntity } from "../profile.entity";

export class UpdateProfileDto implements Partial<ProfileEntity>{
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    city: string;    
}