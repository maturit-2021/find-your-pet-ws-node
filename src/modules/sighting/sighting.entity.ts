import { Geometry } from "geojson";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { LostPetEntity } from "../lost_pet/lostpet.entity";
import { ProfileEntity } from "../profile/profile.entity";

@Entity()
export class SightingEntity{
    @PrimaryColumn()
    @CreateDateColumn()
    createdDate: Date;

    @Column({ 
        type: 'point',
        nullable: false,
        spatialFeatureType: 'Point',
        srid: 4326
    })
    location: Geometry;

    @ManyToOne(_type => ProfileEntity, profile => profile.sightings)
    profile: ProfileEntity;

    @ManyToOne(_type => LostPetEntity, lp => lp.sightings, { primary: true })
    lostPet: LostPetEntity;
}