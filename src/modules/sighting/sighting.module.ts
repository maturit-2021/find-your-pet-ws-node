import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SightingEntity } from './sighting.entity';

@Module({
    imports: [TypeOrmModule.forFeature([SightingEntity])],
    exports: [TypeOrmModule],
    controllers: [],
    providers: [],
})
export class SightingModule {}
