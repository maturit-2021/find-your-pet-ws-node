import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { CommentEntity } from "../comment/comment.entity";
import { ProfileEntity } from "../profile/profile.entity";

@Entity()
export class PostEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @Column({
        type: 'tinytext',
        nullable: false
    })
    content: string;

    @Column()
    image: Buffer;

    @CreateDateColumn()
    createdDate: Date;

    @ManyToOne(_type => ProfileEntity, profile => profile.posts)
    profile: ProfileEntity;

    @OneToMany(_type => CommentEntity, cmt => cmt.post)
    comments: CommentEntity;
}