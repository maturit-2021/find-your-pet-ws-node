import { Body, Controller, Get, Post } from '@nestjs/common';
import { AddUserDto } from './dto/add_user.dto';
import { UserService } from './user.service';

@Controller('users')
export class UsersController {
    constructor(
        private userService: UserService
    ){ }
}
