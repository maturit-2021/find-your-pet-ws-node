import { IsEmail, IsNotEmpty } from "class-validator";

export class AddUserDto {
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}