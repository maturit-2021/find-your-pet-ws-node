import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { ProfileEntity } from '../profile/profile.entity';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @Column({
        length: 64,
        nullable: false,
        unique: true,
    })
    email: string;

    @Column({
        length: 128,
        nullable: false
    })
    password: string;

    @OneToOne(_type => ProfileEntity)
    @JoinColumn()
    profile: ProfileEntity;
}