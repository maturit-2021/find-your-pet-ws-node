import { Body, Controller, Post } from '@nestjs/common';
import { LostPetStatusDto } from './dto/set_pet_lost_status.dto';
import { LostPetService } from './lostpet.service';

@Controller('lost-pet')
export class LostpetController {
    constructor(
        private lostPetService: LostPetService
    ){}

    @Post('update-status')
    updateLostPetStatus(@Body() data: LostPetStatusDto){
        return this.lostPetService.updateStatus(data);
    }
}
