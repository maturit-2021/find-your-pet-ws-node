import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LostPetStatusDto } from './dto/set_pet_lost_status.dto';
import { LostPetEntity } from './lostpet.entity';
import { LostPetStatus } from './model/lostpet.enum';

@Injectable()
export class LostPetService {
    constructor(
        @InjectRepository(LostPetEntity) private lostPetsRepository: Repository<LostPetEntity>,
    ) {}

    /**
     * Find the latest record inserted for the given petId
     * the records will be sorted by creation date
     */
    async findLastByPet(petId: string): Promise<LostPetEntity | undefined>{
        let lostPet = await this.lostPetsRepository.findOne({
            where: { petId },
            order: { createdDate: 'DESC' }
        });

        return lostPet;
    }

    /**
     * Update the status of a pet inside the entity Lost Pet
     * if new status is equal to the one present in the database
     * no update will be made and the old entity will be returned
     */
    async updateStatus({ petId, status }: LostPetStatusDto){
        let lastLostRecord = await this.lostPetsRepository.findOne({
            where: { petId },
            order: { createdDate: 'DESC' }
        });
        
        // If there's a match and previous status was NOT_FOUND
        if (lastLostRecord?.status === LostPetStatus.notFound){
            console.log('aqui eih')
            // If updating to same status NOT_FOUND, then just return same instance
            if (lastLostRecord.status === status)
                return lastLostRecord;

            // Should be updating to FOUND
            lastLostRecord.status = status;
            return this.lostPetsRepository.save(lastLostRecord);
        }

        // If new status is found and old status was found, so just return the old status
        if (lastLostRecord?.status === LostPetStatus.found && status === LostPetStatus.found){
            return lastLostRecord;
        }

        let entity = new LostPetEntity({ petId, status });
        return this.lostPetsRepository.save(entity);
    }
}
