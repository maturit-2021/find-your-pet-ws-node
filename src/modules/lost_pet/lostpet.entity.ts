import { DateTime } from "luxon";
import { dateTimeTransformer } from "src/lib/transformers/date.transformer";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { PetEntity } from "../pet/pet.enitity";
import { SightingEntity } from "../sighting/sighting.entity";
import { LostPetStatus } from "./model/lostpet.enum";

@Entity()
export class LostPetEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @CreateDateColumn({ transformer: dateTimeTransformer })
    createdDate: DateTime;

    @Column("enum", { enum: LostPetStatus })
    status: LostPetStatus;

    @JoinColumn({name: 'petId' })
    @ManyToOne(_type => PetEntity, pet => pet.lostPets, { onDelete: 'CASCADE' })
    pet: PetEntity;

    @Column()
    petId: string;

    @OneToMany(_type => SightingEntity, sg => sg.lostPet)
    sightings: SightingEntity[];

    constructor({petId, status}: Partial<LostPetEntity> = {}) {
        this.petId = petId,
        this.status = status;
    }
}