export enum LostPetStatus {
    notFound = 'not_found',
    found = 'found'
}