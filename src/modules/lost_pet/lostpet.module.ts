import { LostpetController } from './lostpet.controller';
import { LostPetService } from './lostpet.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LostPetEntity } from './lostpet.entity';

@Module({
    imports: [TypeOrmModule.forFeature([LostPetEntity])],
    exports: [
        TypeOrmModule,
        LostPetService
    ],
    controllers: [
        LostpetController,
    ],
    providers: [
        LostPetService,
    ],
})
export class LostPetModule { }
