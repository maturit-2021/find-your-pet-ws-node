import { IsEnum, IsNotEmpty, IsUUID } from "class-validator";
import { LostPetStatus } from "../model/lostpet.enum";

export class LostPetStatusDto{
    @IsNotEmpty()
    @IsUUID()
    petId: string;

    @IsNotEmpty()
    @IsEnum(LostPetStatus)
    status: LostPetStatus;
}