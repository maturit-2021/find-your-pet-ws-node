import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BeaconEntity } from './beacon.entity';

@Module({
    imports: [TypeOrmModule.forFeature([BeaconEntity])],
    exports: [TypeOrmModule],
    controllers: [],
    providers: [],
})
export class BeaconModule {}
