import { CreateDateColumn, Entity, Generated, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { PetEntity } from "../pet/pet.enitity";

@Entity()
export class BeaconEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @JoinColumn()
    @OneToOne(_type => PetEntity, { primary: true })
    pet: PetEntity;

    @CreateDateColumn()
    createdDate: Date;
}