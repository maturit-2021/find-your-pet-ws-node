
import { Injectable, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class WsJwtGuard extends AuthGuard('wsJwtStrategy'){
  constructor(private _reflector: Reflector) {
    super();
  }
  
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }
}
