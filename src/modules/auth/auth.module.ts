import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { UsersModule } from 'src/modules/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { ProfileModule } from '../profile/profile.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { WsJwtStrategy } from './strategies/ws_jwt.strategy';
import { EnvironmentVariables, AuthConfig } from 'src/lib/interfaces/environment.interface';

@Module({
    imports: [
        UsersModule,
        ProfileModule,
        PassportModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService<EnvironmentVariables>) => ({
                secretOrPrivateKey: configService.get<AuthConfig>('auth').secret,
                signOptions: {
                    expiresIn: configService.get<AuthConfig>('auth').duration,
                },
            }),
        }),
    ],
    exports: [
        AuthService
    ],
    controllers: [
        AuthController,
    ],
    providers: [
        AuthService,
        LocalStrategy,
        JwtStrategy,
        WsJwtStrategy
    ],
})
export class AuthModule { }
