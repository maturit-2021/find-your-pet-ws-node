import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UserAuth } from '../dto/jwt_payload.dto';
import { ConfigService } from '@nestjs/config';
import { AuthConfig, EnvironmentVariables } from 'src/lib/interfaces/environment.interface';

@Injectable()
export class WsJwtStrategy extends PassportStrategy(Strategy, 'wsJwtStrategy') {
    constructor(private configService: ConfigService<EnvironmentVariables>) {
        super({
            jwtFromRequest: ExtractJwt.fromExtractors([
                (req) => {
                    const token = (req as any)?.handshake?.headers?.authorization;
                    return token?.replace('Bearer ', '')?.replace('bearer ', '');
                }
            ]),
            ignoreExpiration: false,
            secretOrKey: configService.get<AuthConfig>('auth').secret,
        });
    }

    validate(payload: any): UserAuth {
        return new UserAuth({
            email: payload.email,
            profileId: payload.profileId,
            userId: payload.userId
        });
    }
}