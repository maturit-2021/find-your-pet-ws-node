import { Controller, Post, UseGuards, Request, Body } from '@nestjs/common';
import { Public } from 'src/lib/metada/public.metadata';
import { AuthService } from './auth.service';
import { JWTResponse } from './dto/jwt_response.dto';
import { RegisterDto } from './dto/register.dto';
import { LocalAuthGuard } from './guard/local_auth.guard';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) { }

    @Public()
    @Post('login')
    @UseGuards(LocalAuthGuard)
    async login(@Request() req): Promise<JWTResponse> {
        return this.authService.login(req.user);
    }

    @Public()
    @Post('register')
    async register(@Body() data: RegisterDto): Promise<JWTResponse> {
        const user = await this.authService.register(data);

        return this.authService.login(user);
    }
}
