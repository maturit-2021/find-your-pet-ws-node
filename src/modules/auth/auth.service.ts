import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from 'src/modules/users/user.entity';
import { UserService } from 'src/modules/users/user.service';
import { ProfileService } from '../profile/profile.service';
import { JWTResponse } from './dto/jwt_response.dto';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthService { 
    constructor(
        private userService: UserService,
        private profileService: ProfileService,
        private jwtService: JwtService
    ) {}

    /**
     * Find and return the user with the given email/password if one exists
     */
    async validateUser(email: string, password: string): Promise<UserEntity>{
        const user = await this.userService.findByEmailAndPassword(email, password);
        return user;
    }

    /**
     * Generate the JWT access token for the given user
     * @returns JWT object with the access token for future authentication
     */
    login(user: UserEntity): JWTResponse{
        const payload = {
            userId: user.id,
            profileId: user.profile.id,
            email: user.email,
        };

        return {
            token: this.jwtService.sign(payload),
            email: user.email,
        };
    }

    /**
     * Create a new ProfileEntity and a new UserEntity with the 
     * given data
     */
    async register(data: RegisterDto): Promise<UserEntity> {
        const profile = await this.profileService.createProfile(data);
        return profile;
    }
}
