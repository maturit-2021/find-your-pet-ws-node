import { IsEmail, IsNotEmpty, Max } from 'class-validator';

export class RegisterDto{
    @IsNotEmpty()
    name!: string;

    @IsNotEmpty()
    city!: string;
    
    @IsEmail()
    email!: string;

    @IsNotEmpty()
    password!: string;
}