import { IsEmail, IsNotEmpty, IsUUID } from "class-validator";

/**
 * Contains the main data for the current authenticated user
 * with the information retrieved from the request Jwt Token
 */
export class UserAuth{
    /**
     * The email assigned to the payload of the current
     * authenticated user
     */
    @IsNotEmpty()
    @IsEmail()
    public readonly email: string;

    /**
     * The ID of the current authenticated user profile
     */
    @IsNotEmpty()
    @IsUUID()
    public readonly profileId: string;

    /**
     * The ID of the current authenticated user
     */
    @IsNotEmpty()
    @IsUUID()
    public readonly userId: string;

    constructor({ email, profileId, userId }: Partial<UserAuth>){
        this.email = email;
        this.profileId = profileId,
        this.userId = userId;
    }
}