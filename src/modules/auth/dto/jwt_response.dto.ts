export interface JWTResponse{
    token: string;
    email: string;
}