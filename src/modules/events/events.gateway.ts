import { Logger, UseGuards } from '@nestjs/common';
import {
    ConnectedSocket,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { from} from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { Server, Socket } from 'socket.io';
import { User } from 'src/lib/metada/user.metadata';
import { UserAuth } from '../auth/dto/jwt_payload.dto';
import { WsJwtGuard } from '../auth/guard/ws_jwt.guard';

@UseGuards(WsJwtGuard)
@WebSocketGateway({
    transports: ['websocket', 'polling']
})
export class EventsGateway {
    @WebSocketServer()
    private server: Server;

    private logger = new Logger(this.constructor.name);

    // Handle new socket connection
    handleConnection(client: Socket) {
        this.logger.log(`New Client Connected ${client.id}`);
    }

    // Handle socket disconnect
    handleDisconnect(client: Socket, @User() user: UserAuth) {
        // this.logger.log(`User ${user.profileId} is now disconnected, leaving the room...`)
        // client.leave(user.profileId);
    }

    @SubscribeMessage('room')
    async joinRoomToTrackPets(@User() user: UserAuth, @ConnectedSocket() client: Socket)  {
        this.logger.log(`User ${user.profileId} has joined the room`)
        client.join(user.profileId);
    }
}