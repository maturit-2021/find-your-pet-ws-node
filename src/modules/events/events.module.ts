import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { EventsGateway } from './events.gateway';

@Module({
    imports: [
        AuthModule
    ],
    controllers: [],
    providers: [EventsGateway],
    exports: [EventsGateway]
})
export class EventsModule { }
