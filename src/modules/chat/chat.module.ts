import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatEntity } from './chat.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ChatEntity])],
    exports: [TypeOrmModule],
    controllers: [],
    providers: [],
})
export class ChatModule { }
