import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { ProfileEntity } from "../profile/profile.entity";


@Entity()
export class ChatEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;
    
    @CreateDateColumn()
    createdDate: Date;
    
    @ManyToOne(_type => ProfileEntity, profile => profile.pets, { primary: true })
    profileFrom: ProfileEntity;

    @ManyToOne(_type => ProfileEntity, profile => profile.pets, { primary: true })
    profileTo: ProfileEntity;
    
    @Column({
        type: 'text',
        nullable: false
    })
    message: string;

    @Column({
        length: 32,
        nullable: false,
    })
    messageType: string;    
}