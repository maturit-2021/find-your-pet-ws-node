import { DateTime } from 'luxon';
import { bufferToStringTransformer } from 'src/lib/transformers/buffer_to_string.transformer';
import { dateTimeTransformer } from 'src/lib/transformers/date.transformer';
import { Entity, Column, ManyToOne, CreateDateColumn, OneToMany, JoinColumn, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { LostPetEntity } from '../lost_pet/lostpet.entity';
import { ProfileEntity } from '../profile/profile.entity';

@Entity()
@Unique('idx_petOwner', ['profile', 'name'])
export class PetEntity {
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;
    
    @Column({ length: 128, nullable: false, })
    name: string;

    @CreateDateColumn({
        transformer: dateTimeTransformer
    })
    createdDate: DateTime;

    @Column({
        type: 'mediumblob',
        transformer: bufferToStringTransformer,
        nullable: true,
        default: null,
    })
    image?: Buffer;

    @JoinColumn({ name: 'profileId' })
    @ManyToOne(_type => ProfileEntity, profile => profile.pets, { nullable: false })
    profile: ProfileEntity;

    @Column()
    profileId: string;

    @OneToMany(_type => LostPetEntity, lp => lp.pet, { cascade: true })
    lostPets: LostPetEntity[];

    // Represent the latest record from the LostPet entity
    lostPet?: LostPetEntity;

    constructor({name, image, profileId, profile}: Partial<PetEntity> = {}){
        this.name = name;
        this.image = image;
        this.profileId = profileId;
        this.profile = profile;
    }
}