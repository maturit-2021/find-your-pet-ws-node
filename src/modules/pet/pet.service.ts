import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LostPetService } from '../lost_pet/lostpet.service';
import { AddPetDto } from './dto/add_pet.dto';
import { PetEntity } from './pet.enitity';

@Injectable()
export class PetService {
    constructor(
        @InjectRepository(PetEntity) private petsRepository: Repository<PetEntity>,
        private lostPetService: LostPetService
    ){}

    addNewPet({ name, profileId }: Partial<PetEntity>): Promise<PetEntity> {
        const petEntity = new PetEntity({ name, profileId });
        return this.petsRepository.save(petEntity);
    }

    /**
     * Delete a Pet Entity with the given Id that belongs to the
     * given user Profile ID
     * @param profileId - The ID of the owner
     * @param id - The id of the pet
     * @returns - true in case of affected rows greater then 0
     */
    async deletePetById({id, profileId}: Partial<PetEntity>): Promise<boolean> {
        let res = await this.petsRepository.delete({ id, profileId });
        return res.affected > 0;
    }

    /**
     * Retrieve the list of pets for the given Profile ID
     */
    async petsOfUser({ profileId }: Partial<PetEntity>): Promise<PetEntity[]>{
        const pets = await this.petsRepository.find({
            where: { profileId }
        });
        
        await Promise.all(
            pets?.map(async p => p.lostPet = await this.lostPetService.findLastByPet(p.id))
        );

        return pets ?? [];
    }

    /**
     * Update the pet picture
     * @param id - The pet ID to be updated
     * @param image - The image to be used
     */
    async updatePicture({ id, profileId, image }: Partial<PetEntity>): Promise<void> {
        await this.petsRepository.update({ id, profileId }, { image });
    }
}
