import { IsNotEmpty } from "class-validator";
import { PetEntity } from "../pet.enitity";

export class AddPetDto implements Partial<PetEntity>{
    @IsNotEmpty()
    name: string;
}