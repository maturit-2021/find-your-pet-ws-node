import { IsNotEmpty, IsUUID } from "class-validator";

export class DeletePetDto{
    @IsNotEmpty()
    @IsUUID()
    id: string;
}