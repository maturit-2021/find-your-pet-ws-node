import { Body, Controller, Delete, Get, Param, Post, Put, Query, Req, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from 'src/lib/metada/user.metadata';
import { UserAuth } from '../auth/dto/jwt_payload.dto';
import { AddPetDto } from './dto/add_pet.dto';
import { DeletePetDto } from './dto/delete_pet.dto';
import { PetEntity } from './pet.enitity';
import { PetService } from './pet.service';

@Controller('pet')
export class PetController {
    constructor(
        private petService: PetService
    ){}

    @Get('own')
    getOwnPets(@User() user: UserAuth): Promise<PetEntity[]> {
        return this.petService.petsOfUser(user);
    }

    @Delete()
    deleteUserPet(@User() user: UserAuth, @Body() pet: DeletePetDto): Promise<boolean> {
        return this.petService.deletePetById({ ...user, ...pet });
    }

    @Post()
    addNewPet(@User() user: UserAuth, @Body() pet: AddPetDto): Promise<PetEntity> {
        return this.petService.addNewPet({
            ...pet,
            profileId: user.profileId
        });
    }

    @Put('/:id/picture')
    @UseInterceptors(FileInterceptor('image'))
    updatePetPicture(@Param('id') id: string, @User() user: UserAuth, @UploadedFile() file: Express.Multer.File): Promise<void> {
        return this.petService.updatePicture({
            id,
            profileId: user.profileId,
            image: file?.buffer
        });
    }
}
