import { PetService } from './pet.service';
import { PetController } from './pet.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PetEntity } from './pet.enitity';
import { LostPetModule } from '../lost_pet/lostpet.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([PetEntity]),
        LostPetModule
    ],
    exports: [TypeOrmModule],
    controllers: [
        PetController,
    ],
    providers: [
        PetService,
    ],
})
export class PetModule { }
