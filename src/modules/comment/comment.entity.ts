import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { PostEntity } from "../post/post.entity";
import { ProfileEntity } from "../profile/profile.entity";

@Entity()
export class CommentEntity{
    @PrimaryGeneratedColumn('uuid')
    public readonly id: string;

    @Column()
    comment: string;

    @CreateDateColumn()
    createdDate: Date;

    @ManyToOne(_type =>  PostEntity, post => post.comments)
    post: PostEntity;

    @ManyToOne(_type =>  ProfileEntity, profile => profile.comments)
    profile: ProfileEntity;
}