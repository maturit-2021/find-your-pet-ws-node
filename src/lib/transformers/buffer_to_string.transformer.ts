export const bufferToStringTransformer = {
    to: (i: Buffer) => i,
    from: (i: Buffer) => i?.toString('base64')
}