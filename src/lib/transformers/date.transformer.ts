import { DateTime } from "luxon";

export const dateTimeTransformer = {
    to: (i: DateTime) => (i ?? DateTime.now()).toUTC()?.toJSDate(),
    from: (i: any) => {
        if (typeof i === 'string')
            return DateTime.fromSQL(i);
        else if (i instanceof Date)
            return DateTime.fromJSDate(i);
        else if (typeof i === 'number')
            return DateTime.fromMillis(i);

        return i;
    }
}