import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";

export interface AuthConfig{
    secret: string,
    duration: string,
    duration_refresh: string,
}
export interface DatabaseConfig {
    host: string;
    port: number;
    username: string;
    password: string;
    name: string;
}

export interface EnvironmentVariables {
    environment: 'development' | 'production' | 'test';
    database: DatabaseConfig,
    auth: AuthConfig,
    multerOptions: MulterOptions
}